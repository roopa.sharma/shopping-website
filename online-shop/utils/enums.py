SIZES = (
        ('XS', 'XS'),
        ('Small', 'Small'),
        ('M', 'M'),
        ('L', 'L'),
        ('XL', 'XL')
    )

COLORS = (
        ('Black', 'Black'),
        ('White', 'White'),
        ('Red', 'Red'),
        ('Blue', 'Blue'),
        ('Green', 'Green')
    )

# FILTER_PRICE = {
#         ('$0 to $100', '$0 to $100'),
#         ('$100 to $200', '$100 to $200'),
#         ('$200 to $300', '$200 to $300'),
#         ('$300 to $400', '$300 to $400'),
#         ('$400 to $500', '$400 to $500'),
#
#     }