from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.generic.base import TemplateView



admin.site.site_title = "Online-Shop Administration"
admin.site.site_header = "My Admin"
admin.site.index_title = "Welcome to the Admin Panel"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('shoppingapp.urls')),
]

handler404 = 'online-shop.views.handler404'
handler500 = 'online-shop.views.handler500'

if settings.DEBUG:
	
	urlpatterns += [
		# Testing 404 and 500 error pages
		path('404/', TemplateView.as_view(template_name='404.html'), name='404'),
		path('500/', TemplateView.as_view(template_name='500.html'), name='500'),
		# path('/', TemplateView.as_view(template_name='index.html'), name='home'),
		# path('shop/', TemplateView.as_view(template_name='shop.html'), name='shop'),
		# path('detail/', TemplateView.as_view(template_name='detail.html'), name='detail'),
		# path('pages/', TemplateView.as_view(template_name='cart.html'), name='pages'),
		# path('contact/', TemplateView.as_view(template_name='contact.html'), name='contact'),
	]

	from django.conf.urls.static import static
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
	import debug_toolbar
	urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]
