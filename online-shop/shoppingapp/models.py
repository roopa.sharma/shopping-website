from django.db import models
from django.urls import reverse
from django_extensions.db.fields import AutoSlugField
from django_extensions.db.models import TimeStampedModel
from mptt.models import MPTTModel, TreeForeignKey

from utils import enums


# home page products
class Product(TimeStampedModel):
    product_name = models.CharField(max_length=100)
    images = models.ImageField(upload_to='media', null=True, blank=True)
    price = models.CharField(null=True, max_length=1000)
    is_published = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Product_Image"

    def __str__(self):
        return self.price


# shop page
class Shop(TimeStampedModel):
    product_name = models.CharField(max_length=100)
    images = models.ImageField(upload_to='media', null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)
    colors = models.CharField(max_length=50, null=True, blank=True, choices=enums.COLORS, unique=False)
    slug = AutoSlugField(populate_from=['product_name'], null=True, blank=True)
    catt = models.ForeignKey('ShopCategory', on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name = "Shop_Image"

    def __str__(self):
        return self.product_name

    def get_absolute_url(self):
        return reverse('detail', kwargs={'slug': self.slug})


# home page
class Category(TimeStampedModel):
    category_name = models.CharField(max_length=100)
    images = models.ImageField(upload_to='media', null=True, blank=True)
    product = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Categorie"

    def __str__(self):
        return self.category_name


# detail page
class ShopDescription(TimeStampedModel):
    name = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, max_length=10000)
    price = models.CharField(null=True, max_length=1000)
    information = models.TextField(null=True, max_length=10000)
    sizes = models.CharField(max_length=50, null=True, blank=True, choices=enums.SIZES, unique=True)
    colors = models.CharField(max_length=50, null=True, blank=True, choices=enums.COLORS, unique=True)

    class Meta:
        verbose_name = "ShopDescription"

    def __str__(self):
        return self.description


class CommentReview(TimeStampedModel):
    review = models.TextField()
    name = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    post = models.ForeignKey(Shop, on_delete=models.CASCADE, related_name='comments')

    class Meta:
        verbose_name = "Review"

    def __str__(self):
        return self.name


class Contact(models.Model):
    Name = models.CharField(max_length=100, null=True, blank=True)
    Email = models.EmailField(max_length=100, null=True, blank=True)
    Subject = models.CharField(max_length=215, null=True, blank=True)
    Message = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name_plural = "ContactUs"
        verbose_name = "ContactUs"
        ordering = ["Name", "Email", "Subject", "Message"]

    def __str__(self):
        return self.Name


class NewsLetter(TimeStampedModel):
    email = models.EmailField(max_length=100, null=True, blank=True)

    class Meta:
        verbose_name_plural = "NewsLetter"
        ordering = ["email"]

    def __str__(self):
        return self.email


class ShopCategory(MPTTModel):
    name = models.CharField(max_length=150)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    slug = AutoSlugField(populate_from=['name'], null=True, blank=True)

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name_plural = "shopcategories"
        ordering = ["name"]

    def __str__(self):
        return self.name

    # def get_absolute_url(self):
    #     return reverse('cat', kwargs={'slug': self.slug})
