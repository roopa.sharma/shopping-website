from django.urls import path
from . import views


app_name = 'shoppingapp'

urlpatterns = [
    path('home/', views.Home.as_view(), name='home'),
    path('shop/', views.ShopView.as_view(), name='shop'),
    path('detail/', views.DetailPageView.as_view(), name='detail'),
    path('pages/', views.CartView.as_view(), name='pages'),
    path('checkout/', views.CheckoutView.as_view(), name='checkout'),
    path('search/', views.SearchView.as_view(), name='search'),
    path('contact/', views.ContactFormView.as_view(), name='contact'),
    path('newsletter/', views.NewsLetterView.as_view(), name='newsletter'),
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('signin/', views.Signin.as_view(), name='signin'),
    path('logout/', views.Logout.as_view(), name='logout'),
    path('shop/<slug:slug>/', views.ProductView.as_view(), name='shop_detail'),
    path('postcomment/', views.PostComment.as_view(), name='postcomment'),
    path('shopdetail/<int:Min>/<int:Max>', views.ShopDetailView.as_view(), name='shopdetail'),
    path('color/<str:str>', views.ColorView.as_view(), name='color'),
    path('shopcategory/<slug:slug>/', views.Shoppcategory.as_view(), name='shopcat'),

]

