from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin

from .models import *


class ProductAdmin(admin.ModelAdmin):
    list_display = ("product_name", "images", "price", "created", "modified")


admin.site.register(Product, ProductAdmin)


@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):
    list_display = ("product_name", 'slug')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ("category_name", "images", "product", "created", "modified")


admin.site.register(Category, CategoryAdmin)


class ShopDescriptionAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "price", "created", "modified")


admin.site.register(ShopDescription, ShopDescriptionAdmin)


class CommentReviewAdmin(admin.ModelAdmin):
    list_display = ("name", "review")


admin.site.register(CommentReview, CommentReviewAdmin)


class ContactAdmin(admin.ModelAdmin):
    list_display = ("Name", "Email", "Subject", "Message")


admin.site.register(Contact, ContactAdmin)


class NewsLetterAdmin(admin.ModelAdmin):
    list_display = ("email", "created", "modified")


admin.site.register(NewsLetter, NewsLetterAdmin)


# @admin.register(ShopCategory)
# class ShopCategoryAdmin(admin.ModelAdmin):
#     list_display = ("name",)

@admin.register(ShopCategory)
class ShopModelAdmin(DraggableMPTTAdmin):
    mptt_indent_field = "name"
    list_display = ('tree_actions', 'indented_title', 'slug')
    list_display_links = ('indented_title',)

    # def get_queryset(self, request):
    #     qs = super().get_queryset(request)
    #
    #     # Add cumulative product count
    #     qs = Category.objects.add_related_count(
    #             qs,
    #             Product,
    #             'category',
    #             'products_cumulative_count',
    #             cumulative=True)
    #
    #     # Add non cumulative product count
    #     qs = Category.objects.add_related_count(qs,
    #              Product,
    #              'categories',
    #              'products_count',
    #              cumulative=False)
    #     return qs
    #
    # def related_products_count(self, instance):
    #     return instance.products_count
    # related_products_count.short_description = 'Related products (for this specific category)'
    #
    # def related_products_cumulative_count(self, instance):
    #     return instance.products_cumulative_count
    # related_products_cumulative_count.short_description = 'Related products (in tree)'
