from django import forms
from shoppingapp import models
from django.forms import ModelForm
from django import forms
from django.core import validators
from django.contrib.auth import get_user_model, authenticate

from user.models import User
from .models import *


class CreateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = '__all__'

class Commentreviewform(forms.ModelForm):
    class Meta:
        model = CommentReview
        fields = '__all__'


class ContactCreateForm(forms.Form):
    name = forms.CharField(max_length=255)
    email = forms.EmailField(max_length=255)
    subject = forms.CharField(max_length=100)
    message = forms.CharField(max_length=250)



class NewsForm(forms.ModelForm):
    class Meta:
        model= NewsLetter
        fields = '__all__'

