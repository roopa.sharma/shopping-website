# Generated by Django 3.1 on 2022-09-22 04:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoppingapp', '0002_auto_20220922_0941'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopcategory',
            name='catt',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='shoppingapp.shop'),
        ),
    ]
