import form as form
from django.contrib.auth import authenticate, logout, login
from django.core.mail import send_mail
from django.db.models import Min, Max
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic import FormView, DetailView, CreateView, ListView
from django.views.generic import TemplateView
from django.views.generic.base import View
from mptt.forms import TreeNodeChoiceField

from .models import *
from shoppingapp import models, forms
# from .forms import *

#By Function-based View
"""
def ShopDetailView(request, Min=0, Max=0):
    shop = models.Shop.objects.filter(price__range=(Min, Max))
    context = {'shop':shop}
    return render(request, 'shoppingapp/shop.html', context)
"""

# class SignUp(generic.View):
#
#     def get(self, request, *args, **kwargs):
#         return render(request, "shoppingapp/signup.html")
#
#     def post(self, request, *args, **kwargs):
#         email = request.POST['email']
#         first_name = request.POST['first_name']
#         last_name = request.POST['last_name']
#         password = request.POST['password']
#         password2 = request.POST['password2']
#         user = models.User.objects.filter(email=email)
#         if user:
#             message = "User is already exist"
#             context = {"message": message}
#             return render(request, 'shoppingapp/index.html', context)
#         else:
#             if password == password2:
#                 data = {
#                     "email": email,
#                     "first_name": first_name,
#                     "last_name": last_name,
#                 }
#                 user_obj = models.User.objects.create(**data)
#                 user_obj.set_password(password)
#                 user_obj.save()
#                 return redirect('shoppingapp:home')
#             message = "Password and Re-enter password not matching.."
#             context = {"message": message}
#             return render(request, 'shoppingapp/index.html', context)

# def home(request):
#     return render(request, "shoppingapp/index.html")

# def shop(request):
#     return render(request, "shoppingapp/shop.html")

# def detail(request):
#     return render(request, "shoppingapp/detail.html")

# def cart(request):
#     return render(request, "shoppingapp/cart.html")

# def contact(request):
#     return render(request, "shoppingapp/contact.html")

# def checkout(request):
#     return render(request, "shoppingapp/checkout.html")

#Home Page
class Home(TemplateView):
    template_name = "shoppingapp/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = models.Category.objects.all()
        context['product'] = models.Product.objects.filter(is_published=True)
        context['queryset'] = models.Product.objects.all()
        context['parent_item'] = models.ShopCategory.objects.filter(level=0)[1:]
        context['child_item'] = models.ShopCategory.objects.filter(level=1)
        print(context)
        return context

#Shop Page
class ShopView(TemplateView):
    template_name = "shoppingapp/shop.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # category = models.ShopCategory.objects.filter(subcategory__name__iexact='category')
        my_shop = models.Shop.objects.all()
        context = {'shop': my_shop}
        return context

#Shop Page
class ShopDetailView(ListView):
    template_name = "shoppingapp/shop.html"

    def get(self, request, *args, **kwargs):
        Min = self.kwargs.get('Min')
        Max = self.kwargs.get('Max')

        shop = models.Shop.objects.filter(price__range=(Min, Max))
        context = {"shop": shop}
        return render(request, "shoppingapp/shop.html", context)

#Shop Page
class ColorView(ListView):
    template_name = "shoppingapp/shop.html"

    def get(self, request, *args, **kwargs):
        color = self.kwargs.get('str')
        shop = models.Shop.objects.filter(colors=color)
        context = {"shop": shop}
        return render(request, "shoppingapp/shop.html", context)

#Home Page
class ProductView(DetailView):
    model = Shop
    # queryset = models.Shop.objects.filter()
    # slug_field = 'product_name'
    template_name = "shoppingapp/detail.html"

    # def get_queryset(self):
    #     return models.Shop.objects.filter()

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['queryset'] = models.Shop.objects.filter()
    #     return context

#Detail Page
class DetailPageView(TemplateView):
    template_name = "shoppingapp/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['queryset'] = models.ShopDescription.objects.all()
        return context

#Cart Page
class CartView(TemplateView):
    template_name = "shoppingapp/cart.html"

#Search Page
class SearchView(TemplateView):
    template_name = "shoppingapp/search.html"

    def get(self, request, *args, **kwargs):
        query = request.GET['query']
        queryset = models.Shop.objects.filter(product_name__icontains=query)
        context = {'queryset': queryset}
        return render(request, 'shoppingapp/search.html', context)

class CheckoutView(TemplateView):
    template_name = "shoppingapp/checkout.html"


class SignUp(FormView):
    template_name = 'shoppingapp/signup.html'
    success_url = reverse_lazy('shoppingapp:home')
    form_class = forms.CreateForm

    def form_valid(self, form):
        form.save()
        user = models.User.objects.get(email=form.data.get('email'))
        user.is_active = True
        user.set_password(form.data.get('password'))
        user.save()
        return super(SignUp, self).form_valid(form)


class Signin(generic.View):
    def get(self, request, *args, **kwargs):
        return render(request, "shoppingapp/signin.html")

    def post(self, request, *args, **kwargs):
        email = request.POST["email"]
        password = request.POST["password"]
        user = authenticate(request, email=email, password=password)

        if user is not None:
            login(request, user)
            return redirect('shoppingapp:home')
        else:
            return redirect('shoppingapp:signin')


class Logout(View):
    def get(self, request):
        logout(request)
        return redirect('shoppingapp:home')

#Reviews Start
class PostComment(FormView):
    def get(self, request, *args, **kwargs):
        return render(request, "shoppingapp/index.html")

    def post(self, request, *args, **kwargs):
        if request.method == "POST":
            form = forms.Commentreviewform(request.POST)
            if form.is_valid():
                form.save()
                return redirect("/detail")

class ContactFormView(FormView):
    template_name = "shoppingapp/contact.html"
    success_url = reverse_lazy('shoppingapp:contact')

    def get(self, request, *args, **kwargs):
        return render(request, "shoppingapp/contact.html")

    def post(self, request, *args, **kwargs):

        if request.method == 'POST':
            form = forms.ContactCreateForm(request.POST)
            if form.is_valid():
                name = form.cleaned_data['name']
                email = form.cleaned_data['email']
                subject = form.cleaned_data['subject']
                message = form.cleaned_data['message']

                contact = Contact(Name=name, Email=email, Subject=subject, Message=message)
                contact.save()

            return render(request, "shoppingapp/index.html")


class NewsLetterView(CreateView):
    # form_class = forms.NewsForm
    template_name = "shoppingapp/index.html"
    # success_url = reverse_lazy('shoppingapp:home')


    def post(self, request, *args, **kwargs):

            form = forms.NewsForm(request.POST)
            if form.is_valid():
                email = form.cleaned_data['email']
                form.save()
                send_mail(
                    'Subject here',
                    'Here is the message.',
                    'from@example.com',
                    [f'{email}'],
                    fail_silently=False,
                )
                return redirect('shoppingapp:home')


class Shoppcategory(ListView):
    template_name = "shoppingapp/shopcat.html"
    model = Shop

    def get(self, request, catt=None, *args, **kwargs):
        cat = self.kwargs.get('slug')
        if not cat.isalpha():
            query = models.Shop.objects.filter(catt=cat)
        else:
            query = models.Shop.objects.filter(product_name__icontains=cat)
        context={'query': query}
        return render(request, "shoppingapp/shopcat.html", context)



